# Aws Scheduler

Aws Scheduler is designed to manage existing AWS Infra (start, stop, alert, email, budget follow up).

## Installation

Just clone de repository.

Do not forget to set AWS Credentials in the appropriate windows profile.

## Usage

You can execute the code directly or using the test project to run it.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT Licence](licence.md)